package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_BRANCH_CMD = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMODITY_OUT = "commodity.out";

	//支店定義ファイル読み込み時正規表現
	private static final String BRANCH_LST = "^[0-9]{3}$";

	//商品定義ファイル読み込み時正規表現
	private static final String BRANCH_CMD = "^[a-zA-Z0-9]{8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String MSG_BRANCH = "支店定義ファイル"	;
	private static final String MSG_COMODITY = "商品定義ファイル";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String BRANCH_NO_FORMAT = "の支店コードが不正です";
	private static final String COMODITY_NO_FORMAT = "の商品コードが不正です";
	private static final String DIGIT_10 = "合計金額が10桁を超えました";
	private static final String FILE_FORMAT = "のフォーマットが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> comodityList = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> comoditySales = new HashMap<>();

		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_LST, MSG_BRANCH)) {
			return;
		}
		//商品定義ファイル読み込み処理
		if(!readFile(args[0],  FILE_NAME_BRANCH_CMD, comodityList, comoditySales, BRANCH_CMD, MSG_COMODITY)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		List<File> rcdFiles = new ArrayList<>();
		//ディレクトリのファイル全て
		File[] files = new File(args[0]).listFiles();

		//ディレクトリのファイル全て終わるまで繰り返し
		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				//rcdNameにを格納
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size()-1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}

		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				// 一行ずつ読み込む
				List<String> salesData = new ArrayList<>();
				while((line = br.readLine()) !=  null) {
					salesData.add(line);
				}

				if(salesData.size() != 3) {
					System.out.println(files[i].getName() + FILE_FORMAT);
					return;
				}

				if(!branchNames.containsKey(salesData.get(0))) {
					System.out.println(files[i].getName() + BRANCH_NO_FORMAT);
					return;
				}

				if(!comodityList.containsKey(salesData.get(1))) {
					System.out.println(files[i].getName() + COMODITY_NO_FORMAT);
					return;
				}

				if(!salesData.get(2).matches("^[0-9]+$")) {
					System.out.println(files[i].getName() + FILE_FORMAT);
					return;
				}

				Long saletmp = Long.parseLong(salesData.get(2));
				Long sum = branchSales.get(salesData.get(0));
				sum = sum + saletmp;
				Long comsum = comoditySales.get(salesData.get(1));
				comsum = comsum + saletmp;
				if(sum >= 10000000000L || comsum >= 10000000000L){
					System.out.println(DIGIT_10);
					return;
				}

				branchSales.put(salesData.get(0),sum);
				comoditySales.put(salesData.get(1), comsum);
			} catch(FileNotFoundException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br!=null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMODITY_OUT, comodityList, comoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, String check, String msg) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(msg+FILE_FORMAT);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if((items.length != 2) || (!items[0].matches(check))) {
					System.out.println(msg+FILE_FORMAT);
					return false;
				}

				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);

		}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			FileWriter filewriter = new FileWriter(file, false);
			bw = new BufferedWriter(filewriter);
			for(String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," +  branchSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
